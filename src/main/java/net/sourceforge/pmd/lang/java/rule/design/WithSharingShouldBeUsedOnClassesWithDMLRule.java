/**
 * BSD-style license; for more info see http://pmd.sourceforge.net/license.html
 */
package net.sourceforge.pmd.lang.java.rule.design;

import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTSoqlStatement;
import net.sourceforge.pmd.lang.java.ast.AccessNode;
import net.sourceforge.pmd.lang.java.ast.JavaNode;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;
import net.sourceforge.pmd.lang.java.rule.AbstractStatisticalJavaRule;
import net.sourceforge.pmd.stat.DataPoint;

public class WithSharingShouldBeUsedOnClassesWithDMLRule extends AbstractJavaRule {
	
    // As a Soql DML Statement node is reached, check the parent if class is 'with sharing'
    public Object visit(ASTSoqlStatement node, Object data) {
    	ASTClassOrInterfaceDeclaration pClass = node.getFirstParentOfType(ASTClassOrInterfaceDeclaration.class);
    	if(!pClass.isWithSharing()) {
    		addViolation(data, node);
    	}	
    	return data;
    }
    
    
	
}
