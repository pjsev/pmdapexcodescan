package net.sourceforge.pmd.lang.java.ast;

public class ASTSoqlLiteral extends AbstractJavaTypeNode {
	
	private boolean isSoql;
	
	public ASTSoqlLiteral(int id) {
        super(id);
    }

    public ASTSoqlLiteral(JavaParser p, int id) {
        super(p, id);
    }

    /**
     * Accept the visitor. *
     */
    @Override
    public Object jjtAccept(JavaParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }
    
    public void setSoqlLiteral() {
        this.isSoql = true;
    }

    public boolean isSoqlLiteral() {
        return isSoql;
    }
}
