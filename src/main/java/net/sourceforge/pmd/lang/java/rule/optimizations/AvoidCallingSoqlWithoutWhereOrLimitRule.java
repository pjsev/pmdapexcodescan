package net.sourceforge.pmd.lang.java.rule.optimizations;

import net.sourceforge.pmd.lang.ast.Node;
import net.sourceforge.pmd.lang.java.ast.ASTSoqlStatement;
import net.sourceforge.pmd.lang.java.ast.ASTDoStatement;
import net.sourceforge.pmd.lang.java.ast.ASTForInit;
import net.sourceforge.pmd.lang.java.ast.ASTForStatement;
import net.sourceforge.pmd.lang.java.ast.ASTWhileStatement;

public class AvoidCallingSoqlWithoutWhereOrLimitRule extends AbstractOptimizationRule {
	
	@Override
    public Object visit(ASTSoqlStatement node, Object data) {
        if (hasNoWhereOrLimit(node)) {
            addViolation(data, node);
        }
        return data;
    }
	
	private boolean hasNoWhereOrLimit(ASTSoqlStatement node) {
		String soql = node.getStatement().toLowerCase();
		if(soql.contains("limit") || soql.contains("where")) {
			return false;
		}
		return true;
	}

}
