package net.sourceforge.pmd.lang.java.ast;

public class ASTSoqlStatement extends AbstractJavaNode {

	public ASTSoqlStatement(int id) {
		super(id);
	}
	
	public ASTSoqlStatement(JavaParser p, int id) {
		super(p, id);
	}

	/**
     * Accept the visitor. *
     */
    public Object jjtAccept(JavaParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }
    
    // Retrieve the Soql Statement
    public String getStatement() {
    	String soql = jjtGetLastToken().toString();
    	return soql.substring(0, soql.length()-1);
    }
    
    // Retrieve full line
    public String getFullLine() {
    	return jjtGetLastToken().toString();
    }
}
