/**
 * BSD-style license; for more info see http://pmd.sourceforge.net/license.html
 */
package net.sourceforge.pmd.lang.java.rule.basic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTLiteral;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;
import net.sourceforge.pmd.lang.rule.properties.EnumeratedMultiProperty;

public class AvoidUsingHardCodedSalesforceIdRule extends AbstractJavaRule {

    @Override
    public Object visit(ASTLiteral node, Object data) {
    	Pattern saleforce18 = Pattern.compile(".*\\\'[a-zA-Z0-9]{18}\\\'.*");
    	Pattern saleforce15 = Pattern.compile(".*\\\'[a-zA-Z0-9]{15}\\\'.*");
    	if(node.getImage() != null) {
    		Matcher matcher18 = saleforce18.matcher(node.getImage());
        	Matcher matcher15 = saleforce15.matcher(node.getImage());
        	
        	if(matcher18.matches() || matcher15.matches()) {
        		addViolation(data, node);
        	}
    	}
    	
    	//System.out.println("val: " + node.getImage());
        return data;
    }
}
