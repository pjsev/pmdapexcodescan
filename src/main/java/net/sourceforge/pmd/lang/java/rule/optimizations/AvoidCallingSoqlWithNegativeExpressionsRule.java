package net.sourceforge.pmd.lang.java.rule.optimizations;

import net.sourceforge.pmd.lang.ast.Node;
import net.sourceforge.pmd.lang.java.ast.ASTSoqlStatement;
import net.sourceforge.pmd.lang.java.ast.ASTDoStatement;
import net.sourceforge.pmd.lang.java.ast.ASTForInit;
import net.sourceforge.pmd.lang.java.ast.ASTForStatement;
import net.sourceforge.pmd.lang.java.ast.ASTWhileStatement;

public class AvoidCallingSoqlWithNegativeExpressionsRule extends AbstractOptimizationRule {
	
	@Override
    public Object visit(ASTSoqlStatement node, Object data) {
        if (hasNegativeExp(node)) {
            addViolation(data, node);
        }
        return data;
    }
	
	private boolean hasNegativeExp(ASTSoqlStatement node) {
		String soql = node.getStatement().toLowerCase();
		if(soql.contains("!=") || soql.contains("<>") || soql.contains("not")) {
			return true;
		}
		return false;
	}

}
