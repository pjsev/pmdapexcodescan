package net.sourceforge.pmd.lang.java.rule.optimizations;

import net.sourceforge.pmd.lang.ast.Node;
import net.sourceforge.pmd.lang.java.ast.ASTMemberSelector;
import net.sourceforge.pmd.lang.java.ast.ASTPrimarySuffix;
import net.sourceforge.pmd.lang.java.ast.ASTSoqlStatement;
import net.sourceforge.pmd.lang.java.ast.ASTDoStatement;
import net.sourceforge.pmd.lang.java.ast.ASTForInit;
import net.sourceforge.pmd.lang.java.ast.ASTForStatement;
import net.sourceforge.pmd.lang.java.ast.ASTWhileStatement;

public class AvoidCallingSizeOnSoqlStatementRule extends AbstractOptimizationRule {

	public Object visit(ASTPrimarySuffix node, Object data) {
        String soqlSize = node.jjtGetFirstToken().toString() + node.jjtGetLastToken().toString();
        if (soqlSize.equals(".size")) {
            addViolation(data, node);
        }
        return data;
    }

}
