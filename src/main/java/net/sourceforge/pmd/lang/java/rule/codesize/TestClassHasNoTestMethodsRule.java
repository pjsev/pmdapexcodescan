/**
 * BSD-style license; for more info see http://pmd.sourceforge.net/license.html
 */
package net.sourceforge.pmd.lang.java.rule.codesize;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTResultType;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;

public class TestClassHasNoTestMethodsRule extends AbstractJavaRule {
	
	private ArrayList<String> methods = new ArrayList<String>();
	
	public Object visit(ASTMethodDeclaration node, Object data) {
		ASTClassOrInterfaceDeclaration pClass = node.getFirstParentOfType(ASTClassOrInterfaceDeclaration.class);
		String className = pClass.getImage().toString();
		// get the children under the method declaration
		List<ASTResultType> methodList = node.findChildrenOfType(ASTResultType.class);
		for(ASTResultType rt: methodList) {
			methods.add(rt.jjtGetFirstToken().toString());
		}
		if(className.endsWith("Test") && !methods.contains("testMethod")) {
			addViolation(data, node);
		}
        return data;
    }
	
}
