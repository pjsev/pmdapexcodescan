/**
 * BSD-style license; for more info see http://pmd.sourceforge.net/license.html
 */
package net.sourceforge.pmd.lang.java.rule.naming;

import net.sourceforge.pmd.lang.java.ast.ASTAnnotation;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;

public class UseIsTestAnnotationRule extends AbstractJavaRule {
	
	private boolean isTestAnnotation;
	
	public Object visit(ASTName node, Object data) {
		if((node.jjtGetFirstToken().toString()).equals("isTest")) {
			isTestAnnotation = true;
		}
		return data;
	}
	
	public Object visit(ASTClassOrInterfaceDeclaration node, Object data) {
		String className = node.getImage();
        if (!isTestAnnotation && className.endsWith("Test")) {
            addViolation(data, node);
        }
        return data;
    }
}
