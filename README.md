1. Build instructions:
	- this is a Maven project, and requires Maven plugin for your IDE (Eclipse IDE preferred)
	- Update the project by right-clicking pom.xml > Maven > Update Project, this will install the necessary dependencies
	- To build, right-click on pom.xml > Run as > Maven build > "clean package"
	- Build success will produce a pmd-apex.jar file under the target folder

2. Applying the created jar file
	- Download PMD bin at https://pmd.github.io/ (pmd-bin-5.4.1 as of this writing)
	- Extract the zip to your desired location
	- Copy and paste the created jar file (eg. pmd-apex-5.4.1) into the root/lib folder of the downloaded PMD bin

3. Test Apex Code Scan via Command-Prompt
	- Reference: https://pmd.github.io/pmd-5.4.1/usage/running.html
	- For Windows example: C:\tmp\pmd-bin-5.4.1\pmd\bin>pmd -d <path of file to test>.java -f xml -R rulesets/java/unusedcode.xml